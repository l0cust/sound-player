# Sound Player

This is very simple sound player designated for the game which has had to be created by the society of amateur internet radio.

The sound player is written in the HTML5 and is very simple to use. For now, it is no longer continued since the game development has been dropped. The idea of this engine is to load sound files to the Audio components and manage them according to the timer interval. The timer decides when the given sound track has to be played or paused. For further information see the documentation below.

## License

Currently the project has no license due to its destiny. It would have the same license as the whole game if it would be under active development.

## Documentation

Below sections discover everything what is needed to use this project.

* [API](docs/GameSound.md)
* [Usage](docs/usage.md)
