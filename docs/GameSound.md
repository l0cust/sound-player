# GameSound Object

This object is the entire sound player. It defines methods and properties which work around the Audio object. The files to play are stored in the JSON file in the "files" array. This array is passed to the GameSound's constructor as an argument. See the "Usage" chapter for more information how to use the object and its members.

## Methods

### new GameSound(files)

Creates a GameSound object which manages all sounds. It takes one argument which has to be an array of objects representing each sound and its configuration. This array can be loaded from the JSON file or with the aid of other mechanism. Each array element must contain following properties:

* path - the absolute or relative path to the sound file,
* volume - a decimal floating-point number from 0.0 to 1.0 (greater precision like 0.12 is also allowed),
* loop - wether or not to loop the sound (boolean values - true or false),
* startAt - plays the sound at the given time of timer; the unit depends on interval set with setInterval method,
* endAt (optional) - stops playing the sound at the given time of timer; the unit depends on interval started with setInterval method.

#### Parameters

* files (array) - The files array.

### GameSound.load()

Loads a sound files to their audio objects. It doesn't return anything on success. Otherwise it throws an Error exception due to the verify method usage during loading.

#### Throws

Error when verification of the given file object fails.

### GameSound.changeState()

Reacts on the timer interval. Counts the timer intervals by increasing the timeCount property and changes the playback state by playing or pausing sounds according to the startAt and endAt properties in the particular file object.

### GameSound.pause()

Turns off the timer and pauses all sounds. The playback can be resumed by setting timer interval again (the previously used amount of milliseconds is preferable for playback working properly).

### GameSound.verify()

Verifies the files objects. Used by the load method. Returns true on success or throws an Error exception otherwise.

#### Throws

Error when verification fails.

#### Returns

The boolean true value on success.
----------------------------------

## Properties

* files (array) - The array of files to play.
* sounds (array) - The array of audio objects. Each file gets an individual audio component.
* timeCount (number) - Holds the amount of intervals. If you set your timer interval to 10 milliseconds it measures hundredths of seconds.
* playbackTimer (number) - The timer handle.
--------------------------------------------
