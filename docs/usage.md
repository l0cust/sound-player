# Usage

This section describes how to use the sound player. In the API documentation there is alot of information about array holding all tracks. There is an tip pinting the JSON file as the best way to configure all sounds to play. I will show this example of sound palyer usage but you can load files in any way you like. The most important thing is to fulfil the array element's requirements. All these requirements are described in the GameSound object documentation but they are repeated below in a form of JSON file for your convenience.

## JSON file
Below is the example of JSON file which defines all inported information about files to play.

```JSON
{
 "files": [
  {
   "path": "path/to/file1.mp3",
   "volume": 0.05,
   "loop": false,
   "startAt": 0
  },
  {
  "path": "path/to/file2.mp3",
  "volume": 0.1,
  "loop": true,
  "startAt": 100,
  "endAt": 1000
 },
  {
   "path": "path/to/file3.mp3",
   "volume": 0.15,
   "loop": false,
   "startAt": 200
  }
 ]
}
```

Let's assum that above JSON file is called audio.json.

## Using sound player in our web application - audio.js

To use the GameSound object we have to type our own script which runs the time interval and handles thrown exceptions. For better handling the audio.js file uses the JQuery framework. The playback is run when we press the play button.

```JS
var gs; // The GameSound object
$("#play").click(function(){
 $.getJSON("audio.json").done(function(sounds){
  try{
   gs=new GameSound(sounds.files);
   gs.load(); // Loads listed files to the Audio components.
   gs.playbackTimer=setInterval("gs.changeState()",10); // Setting timer interval to 10 milliseconds will cause measuring in hundredths of seconds.
  }
  catch (Error e) {
   alert(e.message);
  }
 }).fail(function(){
  alert("Unable to load audio files.");
 });
});
```

Finally we have to join audio.js and GameSound.js to the web application where the Play button is created.

## audio.html

```XHTML
<!Doctype HTML>
<html>
 <head>
  <meta charset="utf-8"/>
  <title>Web application with sound player usage</title>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="GameSound.js"></script>
  <script src="audio.js"></script>
 </head>
 <body>
  <button id="play">Play</button>
 </body>
</html>
```
