/**
 * @file This file represents the GameSound object which manages the audio part of the game.
 * @author Artur Rutkowski <locust@mailbox.org>
 */

/**
 * Creates and represents a GameSound object which manages all sounds in the game. It takes one argument which has to be an array of objects representing each sound and its configuration.
 * This array can be loaded from the JSON file or with the aid of other mechanism.
 * Each array element must contain following properties:
 * <ul>
 * <li>path - the absolute or relative path to the sound file,</li>
 * <li>volume - a decimal floating-point number from 0.0 to 1.0 (greater precision like 0.12 is also allowed),</li>
 * <li>loop - wether or not to loop the sound (boolean values - true or false),</li>
 * <li>startAt - plays the sound at the given time of timer; the unit depends on interval set with setInterval method,</li>
 * <li>endAt (optional) - stops playing the sound at the given time of timer; the unit depends on interval started with setInterval method.</li>
 *
 * @constructor
 * @param {array} files - The files array.
 */
function GameSound(files){
 /** @property {array} files - The array of files to play. */
 this.files=files;
 /** @property {array} sounds - The array of audio objects. Each file gets an individual audio component. */
 this.sounds=[];
 /** @property {number} timeCount - Holds the amount of intervals. If you set your timer interval to 10 milliseconds it measures hundredths of seconds. */
 this.timeCount=0;
 /** @property {number} playbackTimer - The timer handle. */
 this.playbackTimer=null;
 /**
  * Loads a sound files to their audio objects. It doesn't return anything on success. Otherwise it throws an Error exception due to the verify method usage during loading.
  *
  * @method GameSound#load
  * @throws {Error} When verification of the given file object fails.
  */
 this.load=function(){
  if(this.verify()){
   for(var i=0;i<this.files.length;i++){
    this.sounds.push(new Audio());
    var pos=this.sounds.length-1;
    this.sounds[pos].src=this.files[i].path;
    this.sounds[pos].volume=this.files[i].volume;
    if(this.files[i].loop) this.sounds[pos].loop=true;
    this.sounds[pos].load();
   }
  }
 };
 /**
  * Reacts on the timer interval.
  * Counts the timer intervals by increasing the timeCount property and changes the playback state by playing or pausing sounds according to the startAt and endAt properties in the particular file object.
  *
  * @method GameSound#changeState
  */
 this.changeState=function(){
  this.timeCount++;
  for(var i=0;i<this.sounds.length;i++){
   if(this.timeCount>=this.files[i].startAt) this.sounds[i].play();
   if(this.files[i].hasOwnProperty("endAt") && this.timeCount>=this.files[i].endAt) this.sounds[i].pause();
  }
 };
 /**
  * Turns off the timer and pauses all sounds. The playback can be resumed by setting timer interval again (the previously used amount of milliseconds is preferable for playback working properly).
  *
  * @method GameSound#pause
  */
 this.pause=function(){
  clearInterval(this.playbackTimer);
  for(var i=0;i<this.sounds.length;i++) this.sounds[i].pause();
 };
 /**
  * Verifies the files objects. Used by the load method. Returns true on success or throws an Error exception otherwise.
  *
  * @method GameSound#verify
  * @throws {Error} When verification fails.
  * @returns {boolean} The true value on success.
  */
 this.verify=function(){
  for(var i=0;i<this.files.length;i++){
   if(!this.files[i].hasOwnProperty("path")) throw new Error("The path property doesn't exist in the object located at "+(i+1)+" position.");
   if(!this.files[i].hasOwnProperty("volume")) throw new Error("The volume property doesn't exist in the object located at "+(i+1)+" position.");
   if(!this.files[i].hasOwnProperty("loop")) throw new Error("The loop property doesn't exist in the object located at "+(i+1)+" position.");
   if(!this.files[i].hasOwnProperty("startAt")) throw new Error("The startAt property doesn't exist in the object located at "+(i+1)+" position.");
  }
  return true;
 };
}
